package mullvad

import (
	"context"
	"encoding/hex"
	"fmt"
	"git.kuschku.de/justjanne/terraform-provider-mullvad/api"
	"git.kuschku.de/justjanne/terraform-provider-mullvad/config"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"golang.org/x/crypto/sha3"
)

func dataSourceWireguardConfig() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceWireguardConfigRead,
		Schema: map[string]*schema.Schema{
			"private_key": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"server": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"peer": &schema.Schema{
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"config": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func dataSourceWireguardConfigRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*config.MullvadContext)

	serverHostname := d.Get("server").(string)
	privateKey, err := api.ParseWireguardKey(d.Get("private_key").(string))
	if err != nil {
		return diag.FromErr(fmt.Errorf("could not parse private key: %w", err))
	}

	data, err := config.GenerateConfig(c, serverHostname, privateKey)
	if err != nil {
		return diag.FromErr(err)
	}

	if err := d.Set("config", data.Config); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("peer", data.Peer); err != nil {
		return diag.FromErr(err)
	}
	d.SetId(generateId(c.AccountId, privateKey))

	return nil
}

func generateId(accountId string, key api.WireguardKey) string {
	h := sha3.New512()
	h.Write([]byte(accountId))
	h.Write([]byte(key.String()))
	sum := h.Sum(nil)
	return hex.EncodeToString(sum)
}

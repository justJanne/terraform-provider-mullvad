package api

import (
	"encoding/json"
	"fmt"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"io/ioutil"
	"net/http"
	"net/netip"
	"net/url"
	"strings"
)

type MullvadClient struct {
	client   *http.Client
	endpoint *url.URL
}

func NewClient(endpoint string, client *http.Client) (MullvadClient, error) {
	cli := MullvadClient{}
	endpointUrl, err := url.Parse(endpoint)
	if err != nil {
		return cli, fmt.Errorf("could not parse endpoint url: %w", err)
	}
	cli.endpoint = endpointUrl
	if client != nil {
		cli.client = client
	} else {
		cli.client = &http.Client{}
	}
	return cli, nil
}

func (cli *MullvadClient) ListServers() (MullvadServerList, error) {
	var list MullvadServerList
	requestUri, err := cli.endpoint.Parse("/public/relays/wireguard/v2/")
	if err != nil {
		return list, fmt.Errorf("could not determine API endpoint: %w", err)
	}
	response, err := cli.client.Get(requestUri.String())
	if err != nil {
		return list, fmt.Errorf("could not GET server list: %w", err)
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return list, fmt.Errorf("could not GET server list: %w", err)
	}
	responseString := string(responseBody)
	err = json.Unmarshal([]byte(responseString), &list)
	if err != nil {
		return list, fmt.Errorf("could not parse server list: %w", err)
	}
	return list, nil
}

func (cli *MullvadClient) GetEndpoints(accountId string, publicKey wgtypes.Key) ([]netip.Prefix, error) {
	requestUri, err := cli.endpoint.Parse("/wg/")
	if err != nil {
		return nil, fmt.Errorf("could not determine API endpoint: %w", err)
	}
	data := url.Values{}
	data.Set("account", accountId)
	data.Set("pubkey", publicKey.String())
	response, err := cli.client.PostForm(requestUri.String(), data)
	if err != nil {
		return nil, fmt.Errorf("could not GET endpoint list: %w", err)
	}
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("could not parse endpoint list: %w", err)
	}
	var prefixes []netip.Prefix
	for _, prefixCidr := range strings.Split(string(responseBody), ",") {
		prefix, err := netip.ParsePrefix(prefixCidr)
		if err != nil {
			return nil, fmt.Errorf("could not parse endpoint list: %w", err)
		}
		prefixes = append(prefixes, prefix)
	}
	return prefixes, nil
}

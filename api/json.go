package api

import (
	"encoding/json"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type WireguardKey struct {
	wgtypes.Key
}

func ParseWireguardKey(s string) (WireguardKey, error) {
	data, err := wgtypes.ParseKey(s)
	return WireguardKey{Key: data}, err
}

func (n *WireguardKey) UnmarshalJSON(bytes []byte) error {
	var data string
	err := json.Unmarshal(bytes, &data)
	if err != nil {
		return err
	}
	n.Key, err = wgtypes.ParseKey(data)
	if err != nil {
		return err
	}
	return nil
}

func (n WireguardKey) MarshalJSON() ([]byte, error) {
	data := n.String()
	return json.Marshal(data)
}

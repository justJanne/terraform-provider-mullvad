package api

import (
	"fmt"
	"net/netip"
	"strings"
)

type MullvadServerList struct {
	Locations map[string]MullvadServerLocation `json:"locations"`
	Wireguard MullvadWireguardServerList       `json:"wireguard"`
}

type MullvadServerLocation struct {
	City      string  `json:"city"`
	Country   string  `json:"country"`
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}

type MullvadWireguardServerList struct {
	PortRanges  [][]uint16                   `json:"port_ranges"`
	IPv4Gateway netip.Addr                   `json:"ipv4_gateway"`
	IPv6Gateway netip.Addr                   `json:"ipv6_gateway"`
	Relays      []MullvadWireguardServerInfo `json:"relays"`
}

func (list *MullvadWireguardServerList) FindServer(hostname string) (MullvadWireguardServerInfo, error) {
	for _, server := range list.Relays {
		if strings.EqualFold(server.Hostname, hostname) {
			return server, nil
		}
	}
	return MullvadWireguardServerInfo{}, fmt.Errorf("could not find server, %d servers found", len(list.Relays))
}

type MullvadWireguardServerInfo struct {
	Hostname         string       `json:"hostname"`
	Active           bool         `json:"active"`
	Owned            bool         `json:"owned"`
	Location         string       `json:"location"`
	Provider         string       `json:"provider"`
	IPv4Endpoint     netip.Addr   `json:"ipv4_addr_in"`
	IPv6Endpoint     netip.Addr   `json:"ipv6_addr_in"`
	Weight           int          `json:"weight"`
	IncludeInCountry bool         `json:"include_in_country"`
	PublicKey        WireguardKey `json:"public_key"`
}

package mullvad

import (
	"context"
	"git.kuschku.de/justjanne/terraform-provider-mullvad/config"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Provider -
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"account": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("MULLVAD_ACCOUNT", nil),
			},
			"api_endpoint": &schema.Schema{
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("MULLVAD_API_ENDPOINT", nil),
			},
		},
		DataSourcesMap: map[string]*schema.Resource{
			"mullvad_wireguard_config": dataSourceWireguardConfig(),
		},
		ConfigureContextFunc: providerConfigure,
	}
}

func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	account := d.Get("account").(string)
	apiEndpointTmp, ok := d.GetOk("api_endpoint")
	apiEndpoint := "https://api.mullvad.net"
	if ok {
		apiEndpoint = apiEndpointTmp.(string)
	}
	c, err := config.NewMullvadContext(account, apiEndpoint)
	if err != nil {
		return nil, diag.FromErr(err)
	}
	return c, nil
}

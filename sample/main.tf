terraform {
  required_providers {
    mullvad = {
      version = "0.1.3"
      source = "git.kuschku.de/justjanne/mullvad"
    }
  }
}

provider "mullvad" {
  account = "example"
}

data "mullvad_wireguard_config" "rtorrent" {
  private_key = "example"
  server = "example"
}
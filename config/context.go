package config

import "git.kuschku.de/justjanne/terraform-provider-mullvad/api"

type MullvadContext struct {
	Client    api.MullvadClient
	AccountId string
}

func NewMullvadContext(accountId string, apiEndpoint string) (*MullvadContext, error) {
	cli, err := api.NewClient(apiEndpoint, nil)
	if err != nil {
		return nil, err
	}
	return &MullvadContext{
		Client:    cli,
		AccountId: accountId,
	}, nil
}

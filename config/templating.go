package config

import (
	_ "embed"
	"encoding"
	"fmt"
	"git.kuschku.de/justjanne/terraform-provider-mullvad/api"
	"net/netip"
	"strings"
	"text/template"
)

var (
	//go:embed templates/wireguard_config.ini.tmpl
	tmplData string
	tmpl     = template.Must(
		template.New("wireguard_config").Funcs(map[string]any{
			"joinAddr":     joinStrings[netip.Addr],
			"joinPrefix":   joinStrings[netip.Prefix],
			"joinAddrPort": joinStrings[netip.AddrPort],
		}).Parse(tmplData),
	)
)

type configData struct {
	InterfacePrivateKey api.WireguardKey
	InterfaceAddress    []netip.Prefix
	InterfaceGateways   []netip.Addr
	PeerEndpoint        netip.AddrPort
	PeerPublicKey       api.WireguardKey
}

func templateConfig(data configData) (string, error) {
	buffer := strings.Builder{}
	tmpl.Option()
	if err := tmpl.Execute(&buffer, data); err != nil {
		return "", err
	}
	return buffer.String(), nil
}

func toString(data any) string {
	asString, ok := data.(string)
	if ok {
		return asString
	}
	asStringer, ok := data.(*fmt.Stringer)
	if ok {
		return (*asStringer).String()
	}
	asMarshaler, ok := data.(*encoding.TextMarshaler)
	if ok {
		marshalled, err := (*asMarshaler).MarshalText()
		if err == nil {
			return string(marshalled)
		}
	}
	return fmt.Sprint(data)
}

func joinStrings[T any](delimiter string, data []T) string {
	var entries []string
	for _, entry := range data {
		entries = append(entries, toString(entry))
	}
	return strings.Join(entries, delimiter)
}
